r-bioc-dnacopy (1.80.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 11 Jan 2025 17:32:47 +0100

r-bioc-dnacopy (1.80.0-1) experimental; urgency=medium

  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 25 Nov 2024 21:41:18 +0100

r-bioc-dnacopy (1.78.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 16 Aug 2024 13:30:40 +0200

r-bioc-dnacopy (1.78.0-1~0exq) UNRELEASED; urgency=medium

  * Team upload.
  * d/control: move unmanaged build-dep, architecture-is-64-bit, to the
    end of the list so that dh-update-R doesn't ignore the r-* deps.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 06 Aug 2024 13:56:40 +0200

r-bioc-dnacopy (1.78.0-1~0exp) experimental; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Archive.
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 16:33:45 +0200

r-bioc-dnacopy (1.76.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 29 Nov 2023 20:43:25 +0100

r-bioc-dnacopy (1.74.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Drop debian/tests/* in favour of Testsuite: autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Thu, 20 Jul 2023 15:36:13 +0200

r-bioc-dnacopy (1.72.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 21 Jan 2023 14:54:13 +0100

r-bioc-dnacopy (1.72.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 19 Dec 2022 16:45:42 +0100

r-bioc-dnacopy (1.72.1-1) unstable; urgency=medium

  * Team upload.
  * Disable reprotest
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 05 Dec 2022 13:47:38 +0100

r-bioc-dnacopy (1.72.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Sun, 20 Nov 2022 10:41:33 +0100

r-bioc-dnacopy (1.70.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Set upstream metadata fields: Contact.

 -- Andreas Tille <tille@debian.org>  Thu, 12 May 2022 11:06:30 +0200

r-bioc-dnacopy (1.68.0-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 1.68.0

 -- Nilesh Patra <nilesh@debian.org>  Wed, 24 Nov 2021 15:26:12 +0530

r-bioc-dnacopy (1.66.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 01 Sep 2021 20:44:45 +0200

r-bioc-dnacopy (1.64.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Sun, 01 Nov 2020 09:15:27 +0100

r-bioc-dnacopy (1.62.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Thu, 14 May 2020 15:51:03 +0200

r-bioc-dnacopy (1.60.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Fixed debian/watch for BioConductor
  * debhelper-compat 12
  * Standards-Version: 4.4.1

 -- Dylan Aïssi <daissi@debian.org>  Sun, 10 Nov 2019 15:54:41 +0100

r-bioc-dnacopy (1.58.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Wed, 03 Jul 2019 09:47:25 +0200

r-bioc-dnacopy (1.56.0-1) unstable; urgency=medium

  * Team upload.

  [ Jelmer Vernooĳ ]
  * Use secure URI in Homepage field.

  [ Dylan Aïssi ]
  * New upstream version
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Thu, 15 Nov 2018 07:25:59 +0100

r-bioc-dnacopy (1.54.0-2) unstable; urgency=medium

  * Team upload.
  * Fix Maintainer and Vcs-fields

 -- Andreas Tille <tille@debian.org>  Sun, 06 May 2018 07:07:16 +0200

r-bioc-dnacopy (1.54.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Wed, 02 May 2018 11:16:06 +0200

r-bioc-dnacopy (1.52.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version
  * Add registry entry

 -- Andreas Tille <tille@debian.org>  Fri, 10 Nov 2017 09:29:22 +0100

r-bioc-dnacopy (1.50.1-1) unstable; urgency=medium

  * Team upload
  * New upstream version
  * Secure URI in watch file
  * debhelper 10
  * Standards-Version: 4.1.1
  * Add debian/README.source to document binary data files

 -- Andreas Tille <tille@debian.org>  Sun, 22 Oct 2017 18:29:53 +0200

r-bioc-dnacopy (1.48.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version
  * Convert to dh-r

 -- Andreas Tille <tille@debian.org>  Fri, 28 Oct 2016 08:15:20 +0200

r-bioc-dnacopy (1.46.0-1) unstable; urgency=medium

  * Initial release. (Closes: #828189)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 25 Jun 2016 13:58:30 -0700
